import { Nip } from './../../entity/Nip';
import { NipProvider } from './../../providers/nip-provider';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  public nips:any;
  public nip = new Nip();
  public searchInput:string;

  constructor(public navCtrl: NavController, public nipService: NipProvider) {
      this.nipService.getNips().then((data)=>{
        this.nips = data;
      })

  }


  public searchNip(event){
    this.nipService.searchNip(event).then(resultado => this.nips =  resultado.docs);
  }

  public editar(nip){
    this.nip = nip;
  }
  
  public deletar(nip){
    this.nipService.removeNip(nip);
  }



}
