import { Nip } from './../../entity/Nip';
import { NipProvider } from './../../providers/nip-provider';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Dialogs } from '@ionic-native/dialogs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [Geolocation, Camera, Dialogs]
})
export class HomePage {

  public nips: any;
  public nip = new Nip();
  public options: CameraOptions = {
    quality: 80,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  constructor(
    public navCtrl: NavController,
    public nipService: NipProvider,
    private geolocation: Geolocation,
    private camera:Camera,
    private dialog:Dialogs
  ) {

  }

  ionViewDidLoad() {

    this.nipService.getNips().then((data) => {
      this.nips = data;
    });

  }

  public editar(nip){
    this.nip = nip;
  }

  public deletar(nip){
    this.nipService.removeNip(nip);
  }

  public salvarNip() {
      this.geolocation.getCurrentPosition().then((resp) => {
      this.nip.latitude = resp.coords.latitude;
      this.nip.longitude = resp.coords.longitude;

      this.nipService.createNip(this.nip);
      this.dialog.alert('Nip salvo com sucesso!');
      this.navCtrl.setRoot(HomePage);
      //this.navCtrl. (HomePage);
      //window.location.href = '/index.html';
     }).catch((error) => {
       console.log('Erro ao pegar localização', error);
     });

  };

  public salvarUsuario() {
    this.geolocation.getCurrentPosition().then((resp) => {

      this.nip.latitude = resp.coords.latitude;
      this.nip.longitude = resp.coords.longitude;

      this.nipService.createNip(this.nip);
      this.dialog.alert('NIP salvo com sucesso!');
     }).catch((error) => {
       console.log('Erro ao pegar localização', error);
     });

  };

  public uploadImagem(){

    this.camera.getPicture(this.options).then((imageData) => {
     let base64Image = 'data:image/jpeg;base64,' + imageData;
     this.nip.foto = base64Image;
     this.dialog.alert('Upload feito com sucesso!');
    }, (err) => {
     // Handle error
     console.log(err);
    });
  }

}
