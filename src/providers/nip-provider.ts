import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
import { Platform } from 'ionic-angular';
PouchDB.plugin(PouchDBFind);

@Injectable()
export class NipProvider {
  
  
  public data: any;
  public db: any;
  public remote: any;

  constructor() {
    this.db = new PouchDB("nip");
    this.remote = 'http://108.167.148.167:5984/nip';

    let options = {
      live: true,
      retry: true,
      continuous: true,
    };

    this.db.sync(this.remote, options);

  }

  public searchNip(nip: string) {
    return this.db.find({
      selector: { nip: { $regex: nip} }
    })
  }

  public createNip(nip: any) {
    this.db.post(nip);
  }

  public updateNip(nip: any) {

    this.db.put(nip).catch((err) => {

    })

  }

  public removeNip(nip: any) {
    this.db.remove(nip);
  }

  public getNips() {

    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve =>
      this.db.allDocs({
        include_docs: true
      }).then((result) => {
        this.data = [];

        let docs = result.rows.map((row) => {
          this.data.push(row.doc);
        });

        resolve(this.data);

        this.db.changes({ live: true, since: 'now', include_docs: true }).on('change', (change) => {
          this.handleChange(change);
        });

      })
    );

  }

  handleChange(change) {
    let changedDoc = null;
    let changedIndex = null;

    this.data.forEach((doc, index) => {
      if (doc._id === change.id) {
        changedDoc = doc;
        changedIndex = index;
      }
    });

    //Documento deletado
    if (change.deleted) {
      this.data.splice(changedIndex, 1);
    } else {
      if (changedDoc) {
        //Documento atualizado
        this.data[changedIndex] = change.doc;
      } else {
        //Documento adicionado
        this.data.push(change.doc);
      }
    }
  }

}
